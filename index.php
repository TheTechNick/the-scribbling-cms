<?php

define('ROOT_DIR', 		realpath(dirname(__FILE__)) .'/');
define('CONTENT_DIR', 	ROOT_DIR .'content/');
define('LIB_DIR', 		ROOT_DIR .'lib/');
define('THEMES_DIR', 	ROOT_DIR .'themes/');
define('CACHE_DIR', 	LIB_DIR .'cache/');

require_once(ROOT_DIR .'lib/autoloader.php');

$loader = new Autoloader();
$loader->register();

// register the base directories for the namespace prefix
$loader->addNamespace('cms', 'lib');
$loader->addNamespace('Netcarver\Textile', 'lib/vendor/php-textile/src/Netcarver/Textile');
$loader->addNamespace('Symfony\Component\Yaml', 'lib/vendor/yaml');

$cms = new \cms\core\CMS();