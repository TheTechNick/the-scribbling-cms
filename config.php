<?php 
return [
	'base_url' => '',
	'theme' => 'default',
	'language' => [
		'enable' => 	true,
		'default' => 	'en',
		'supported' => 	['en','de'],
	],
];