<?php

namespace cms\core;

use Symfony\Component\Yaml\Yaml;
use cms\helper\FileHelper;
use cms\helper\UrlManager;

class Page extends \cms\base\Object {

    /**
     * Absolute path to this page file
     */
    protected $path;

    /**
     * meta informations
     */
    private $meta;

    /**
     * raw page content
     */
    private $content;

    /**
     * page divided into blocks
     */
    private $blocks;

    /**
     * url to the page
     */
    private $url;


    /**
     * Finds a page for the given url
     * @param $url relative url to the page
     * @return \cms\core\Page | null
     */
    public static function find($url) {
        return UrlManager::urlToPage($url);
    }


    public function getPath() {
        return $this->path;
    }


    /**
     * Get the metadata of the page
     * @return array metadata
     */
    public function getMeta()
    {
        if (!$this->meta) {
            $this->meta = Yaml::parse(trim(substr($this->getContent(), 0, strpos($this->getContent(), '==='))));
        }
        return $this->meta;
    }


    /**
     * Get the Content of the Page
     * @return string the content of the file
     */
    public function getContent()
    {
        if (!$this->content) {
            $this->content = file_get_contents($this->path);
        }
        return $this->content;
    }


    /**
     * Get the content divided into blocks
     */
    public function getBlocks()
    {
        if (!$this->blocks) {
            $this->blocks = [];
            preg_match_all('/===.+?===/s', $this->getContent(), $matches);
            foreach ($matches[0] as $match) {
                //strip === before and after
                $match = substr($match, 3, strlen($match)-6);
                $title = trim(substr($match, 0, strpos($match, "\n")));
                $match = substr($match, strpos($match, "\n"));

                $this->blocks[$title] = $match;
            }
        }
        return $this->blocks;
    }


    /**
     * Get the url to this page
     */
    public function getUrl() {
        if (!$this->url) {
            $this->url = UrlManager::filepathToUrl($this->path);
        }
        return $this->url;
    }

    /**
     * Get the url to this page for a given language
     */
    public function urlForLanguage($lang) {
        $path = substr(str_replace('.txt', '', $this->path), 0, -2) . $lang . '.txt';
        return UrlManager::filepathToUrl($path);
    }
}