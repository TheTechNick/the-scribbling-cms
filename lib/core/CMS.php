<?php

namespace cms\core;

use cms\helper\UrlManager;

/**
 * CMS
 *
 * @author Nico Schieder
 * @license http://opensource.org/licenses/MIT
 * @version 0.1
 */
class CMS {

	/**
	 * CMS Configuration
	 */
	public static $config;

	/**
	 * CMS Language
	 */
	public static $language;

	/**
	 * CMS Requested page
	 */
	public static $url;

	function __construct() {
		$this->loadConfig();

		self::$url = UrlManager::getRequestUrl();
		if (in_array(substr(self::$url,0,2), self::$config['language']['supported'])) {
			self::$language = substr(self::$url,0,2);
			self::$url = substr(self::$url, 3);
		}
		if (self::$config['language']['enable'] && !self::$language) {
			self::$language = $this->detectAcceptLanguage(self::$config['language']['supported'])[0];
			UrlManager::redirect(self::$config['base_url'] . self::$language . '/' . self::$url);
		}

		//render page content
		$page = Page::find(self::$url);

		$view = new View([
			'page' => $page
		]);

		echo $view->render([
			'config' 	=> self::$config,
			'language' 	=> self::$language,
			'url' 		=> self::$url,
		]);
	}

	private function loadConfig() {
		//load config
		self::$config = require(ROOT_DIR . '/config.php');
		if (!isset(self::$config['base_url']) || (self::$config['base_url'] == '')) {
			self::$config['base_url'] = UrlManager::getBaseUrl();
		}
	}

	/**
	 * Detects the clients accept language
	 * @param $available_languages
	 * @return array an list of available languages
	 */
	private function detectAcceptLanguage(array $available_languages) {
		$available_languages = array_flip($available_languages);

		$langs;
		preg_match_all('~([\w-]+)(?:[^,\d]+([\d.]+))?~', strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']), $matches, PREG_SET_ORDER);
		foreach($matches as $match) {
			list($a, $b) = explode('-', $match[1]) + array('', '');
			$value = isset($match[2]) ? (float) $match[2] : 1.0;

			if(isset($available_languages[$match[1]])) {
				$langs[$match[1]] = $value;
				continue;
			}

			if(isset($available_languages[$a])) {
				$langs[$a] = $value - 0.1;
			}
		}
		arsort($langs);
		//changes the output
		foreach ($langs as $key => $value) {
			$output[] = $key;
		}

		return $output;
	}
}