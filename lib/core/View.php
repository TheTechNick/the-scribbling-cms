<?php

namespace cms\core;

use cms\helper\Parser;
use Symfony\Component\Yaml\Yaml;

class View extends \cms\base\Object {

	/**
	 * Theme to use
	 */
	private $theme = 'default';

	/**
	 * Layout to use
	 */
	private $layout = 'default';

	/**
	 * Template to use
	 */
	private $template = 'default';

	/**
	 * The page object
	 */
	protected $page;

	/**
	 * Context to render the php files
	 */
	private $context;

	/**
	 * Renders a php file in the given context
	 */
	private function renderPhpFile($file, $context) {
		ob_start();
		extract(array_merge_recursive($this->context, $context));
		require($file);
		return ob_get_clean();
	}

	/**
	 * Render this view
	 * @param $context variables to make available local ($var)
	 * @return string the rendered content
	 */
	public function render($context = []) {
		$this->context = $context;

		//Set theme
		if (CMS::$config['theme'])
			$this->theme = CMS::$config['theme'];

		if (isset($this->page->meta['Template']))
			$this->template = $this->page->meta['Template'];

		if (isset($this->page->meta['Layout']))
			$this->layout = $this->page->meta['Layout'];

		//Render the blocks of the page
		$rendered_blocks = [];
		foreach ($this->page->blocks as $title => $block) {
			// 1 Replace partials
			$block = $this->replacePartials($block);
			// 2 parse textile
			$block = Parser::parseTextile($block);

			$rendered_blocks[$title] = $block;
		}

		//Template
		$rendered = $this->renderPhpFile(THEMES_DIR . $this->theme . '/templates/' . $this->template . '.php', $rendered_blocks);

		//Layout
		$rendered = $this->renderPhpFile(THEMES_DIR . $this->theme . '/layouts/' . $this->layout . '.php', ['content' => $rendered]);

		return $rendered;
	}


	/**
	 * Replaces an input block with the Content Element
	 * Format:
	 * +++ CE-Title
	 * 	{JSON}
	 * +++
	 * @return string the parsed content
	 */
	private function replacePartials($string) {
		preg_match_all('/\+\+\+.+?\+\+\+/s', $string, $matches);
		foreach ($matches[0] as $match) {
			//strip ===
			$match = substr($match, 3, strlen($match)-6);
			$title = trim(substr($match, 0, strpos($match, "\n")));
			$match = substr($match, strpos($match, "\n"));

			$config = Yaml::parse($match);

			$ce = 'notextile. ' . str_replace(["\n","\r"], '', $this->insertPartial($title, $config));
			$string = preg_replace('/\+\+\+.+?\+\+\+/s', $ce, $string, 1);		
		}
		return $string;
	}


	/**
	 * inserts a content element
	 * @param $ce the name of the ce
	 * @param $config the configuration/content of the ce
	 * @return string html
	 */
	private function insertPartial($ce, $config = []) {
		$file = THEMES_DIR . $this->theme . '/partials/' . $ce . '.php';
		if (is_readable($file)) {
			return $this->renderPhpFile($file, $config);
		}
		return '<div><b style="color: red;">Content Element "'.$ce.'"not found</b></div>';
	}
}