<?php

namespace cms\helper;

class FileHelper {

    /**
     * Checks if a file exists
     * @param $file array|string path to the file or an filepath array
     * @return string|false the existing path or false
     */
    public static function exists($file)
    {
        if (!is_array($file)) {
            $file[] = $file;
        }

        foreach ($file as $path) {
            if (file_exists($path)) {
                return $path;
            }
        }

        return false;
    }

    /**
     * Checks all posible combinations and returns the path to the first match
     * @param $folder folder to operate in
     * @param $extension array fileextensions to check
     * @return string|false the first matching existing file or false
     */
    public static function checkFileExtensions($filepath, $extensions)
    {
        foreach ($extensions as $extension) {
            $file = $filepath . $extension;
            if (file_exists($file)) {
                return $file;
            }
        }
        
        return false;
    }

    /**
     * Helper function to recusively get all files in a directory
     *
     * @param string $directory start directory
     * @param string $ext optional limit to file extensions
     * @param string $max_lvl max folder depth to get
     * @return array the matched files
     */ 
    public static function getFiles($directory, $ext = '', $max_lvl = 30, $lvl = 0)
    {
        $array_items = array();
        if($handle = opendir($directory)){
            while(false !== ($file = readdir($handle))){
                if(preg_match("/^(^\.)/", $file) === 0){
                    if(is_dir($directory. "/" . $file) && ($max_lvl !== -1)){
                        if ($lvl < $max_lvl)
                            $array_items = array_merge($array_items, self::getFiles($directory. "/" . $file, $ext, $max_lvl, $lvl++));
                    } else {
                        $file = $directory . "/" . $file;
                        if(!$ext || strstr($file, $ext))
                            $array_items[] = preg_replace("/\/\//si", "/", $file);
                    }
                }
            }
            closedir($handle);
        }

        return $array_items;
    }














    /**
     * Get the content of a file
     */
    public static function getFileContent($file) {
        if(file_exists($file))
            return file_get_contents($file);
        return false;
    }

    /**
     * Returns the metadata of an file
     * @param $file path to the file
     * @return array | null
     */
    public static function getMetadata($file) {
        return Parser::parseMetadata(Parser::parseContentBlocks(self::getFileContent($file), true));
    }
}