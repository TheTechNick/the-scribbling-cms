<?php

namespace cms\helper;

class Parser {

	/**
	 * Textile parser
	 */
	private static $TextileParser;

	/**
	 * returns the parsed html
	 * @return string
	 */
	public static function parseTextile($string) {
		return self::getTextileParser()->setDocumentType('html5')->parse($string);
	}

	/**
	 * Returns the Textile Parser
	 */
	public static function getTextileParser() {
		if (!self::$TextileParser) {
			self::$TextileParser = new \Netcarver\Textile\Parser();
		}
		return self::$TextileParser;
	}
}