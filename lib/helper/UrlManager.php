<?php

namespace cms\helper;

use cms\core\CMS;
use cms\core\Page;

class UrlManager {

	/**
	 * Determine file language
	 * /path/to/cms/content/page/subpage.en.txt -> en
	 * /path/to/cms/content/page/subpage.txt -> default language
	 * @param $filepath
	 * @return string language key
	 */
	public static function languageFromFilepath($filepath) {
		$language = substr(str_replace('.txt', '', str_replace(CONTENT_DIR, '', $filepath)), -3);
		if (substr($language, 0, 1) === '.') {
			return substr($language, 1);
		}
		return CMS::$config['language']['default'];
	}

	/**
	 * Converts an relative filepath back into an request url
	 * /path/to/cms/content/page/subpage.txt 		-> page/subpage
	 * /path/to/cms/content/page/subpage.de.txt  -> de/page/subpage
	 * @param $filepath path to the file
	 * @return relative url
	 */
	public static function filepathToUrl($filepath)
	{
		$url = str_replace(CONTENT_DIR, '', $filepath);

		if (CMS::$config['language']['enable']) {
			$language = self::languageFromFilepath($filepath);

			// language
			$language_url = '';
			$segments = explode('/', $url);
			$segment_count = count($segments);
			foreach ($segments as $key => $segment) {
				if ($key + 1 === $segment_count) {
					// last segment
					$page = new Page(['path' => $filepath]);
					if (isset($page->meta['URL'])) {
						$segment = $page->meta['URL'];
					} else {
						$segment = substr($segment, 0, strpos($segment, '.'));
					}

					//home is not needed
					if (!($segment === 'home' && $segment_count === 1))
						$language_url .= $segment;

				} else {
					// other segments
					$path = CONTENT_DIR . $language_url . $segment . '.' . $language . '.txt';
					$page = new Page(['path' => $path]);
					if (isset($page->meta['URL'])) {
						$segment = $page->meta['URL'];
					}
					$language_url .= $segment . '/';
				}
			}

			return $language . '/' .$language_url;
		} else {
			// no language
			return substr($url, 0, strpos($url, '.'));
		}








		if ($lang)
			$lang = CMS::$language;

		//strip content directory
		$raw_url = str_replace(CONTENT_DIR, '', $filepath);

		$url = '';
		if (CMS::$config['language']['enable'] && $lang !== CMS::$config['language']['default']) {
			// analyse
			$segments = explode('/', $raw_url);
			$segment_count = count($segments);
			foreach ($segments as $key => $segment) {
				if ($key + 1 === $segment_count) {
					//last file
					$page = new \cms\core\Page(['path' => $filepath]);
					if (isset($page->meta['URL'])) {
						$segment = $page->meta['URL'];
					}

					$url .= $segment;
				} else {
					// folder
					
						$page = new \cms\core\Page(['path' => $segment . '.' . $lang . '.txt']);
						if (isset($page->meta['URL'])) {
							$segment = $page->meta['URL'];
						}

					$url .= $segment . '/';
				}
			}
		} else {
			$url = substr($raw_url, 0, strpos($raw_url, '.'));
		}

		return $url;
	}

	/**
	 * Finds a page for the requested url
	 * page/subpage -> /path/to/cms/content/page/subpage.txt
	 * 			or  -> /path/to/cms/content/page/subpage/index.txt
	 * @param $url request url
	 * @return \cms\core\page | false if not found
	 */
	public static function urlToPage($url)
	{
        if ($url == '')
            $url .= 'home';

        $page = false;

        //
        // 1 check if a matching file exists
        //
        $extensions[] = '.txt';
        //check languages files .en.textlie
        if (CMS::$config['language']['enable']) {
            $extensions[] = '.' . CMS::$language . '.txt';
            $extensions[] = '.' . CMS::$config['language']['default'] . '.txt';
        }
        if ($page_path = FileHelper::checkFileExtensions(CONTENT_DIR . $url, $extensions)) {
        	$page = new Page(['path' => $page_path]);
        }


        //
        // 2 check for a custom url
        //
        if (!$page && CMS::$config['language']['enable']) {

            // path start
            $segment_path = CONTENT_DIR;
            $segments = explode('/', $url);
            $segment_count = count($segments);

            foreach ($segments as $key => $segment) {

                if ($key + 1 === $segment_count) {
                    // last segment
                    if ($new_segment_path = FileHelper::checkFileExtensions($segment_path . $segment, $extensions)) {
                        $segment_path = $new_segment_path;
                        break;
                    }

                    $files = FileHelper::getFiles($segment_path, '.' . CMS::$language . '.txt', 0);
                    foreach ($files as $file) {
                        $found_page = new Page(['path' => $file]);
                        if (isset($found_page->getMeta()['URL']) && $found_page->getMeta()['URL'] == $segment) {
                            //override segment_path

                            $segment_path = $file;
                            break;
                        }
                    }


                } else {
                    // other segments

                    if (is_dir($segment_count . $segment)) {
                        // segment exists
                        $segment_path .= $segment . '/';
                        continue;

                    } else {
                        // segment not found

                        $files = FileHelper::getFiles($segment_path, '.' . CMS::$language . '.txt', 0);
                        foreach ($files as $file) {
                            $found_page = new Page(['path' => $file]);
                            if (isset($found_page->getMeta()['URL']) && $found_page->getMeta()['URL'] == $segment) {
                                //override segment_path

                                $segment_path = substr($file, 0, strpos($file,'.')). '/';
                                break;
                            }
                        }

                    }

                }
            }

            if (is_file($segment_path)) {
                $page = $found_page;
            }
        }
        

        //
        // 3 404
        //
        if (!$page) {
            
            if (CMS::$config['language']['enable']) {
                $path_404 = CONTENT_DIR . '404.' . CMS::$language . '.txt';
            } 
            if (!file_exists($path_404)) {
                $path_404 = CONTENT_DIR . '404.txt';
            }

            $page = new Page(['path' => $path_404]);
        }

        return $page;
	}

	/**
	 * Returns the requested url
	 * www.example.com/page/subpage/testpage -> page/subpage/testpage
	 * @return string request url
	 */
	public static function getRequestUrl()
	{
		// Get request url and script url
		$request_url = (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';
		$script_url  = (isset($_SERVER['PHP_SELF'])) ? $_SERVER['PHP_SELF'] : '';

		// Get our url path and trim the / of the left and the right
		if($request_url != $script_url) $url = trim(preg_replace('/'. str_replace('/', '\/', str_replace('index.php', '', $script_url)) .'/', '', $request_url, 1), '/');
		$url = preg_replace('/\?.*/', '', $url); // Strip query string

		return $url;
	}

	/**
	 * Returns the base url of the site
	 *
	 * @return string base url
	 */
	public static function getBaseUrl()
	{
		$request_url = self::getRequestUrl();
		$protocol = self::getProtocol();
		$base_url = rtrim(str_replace($request_url, '', $protocol . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']));
		if (substr($base_url, -2) === '//') {
			$base_url = substr($base_url, 0, strlen($base_url)-1);
		}
		return $base_url;
	}

	/**
	 * Tries to guess the server protocol.
	 * @return string the current protocol
	 */
	public static function getProtocol()
	{
		$protocol = 'http';
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off'){
			$protocol = 'https';
		}
		return $protocol;
	}

	/**
	 * Redirects to the given url
	 * @param $url
	 */
	public static function redirect($url) {
		header("Location: ".$url);
		die();
	}
}