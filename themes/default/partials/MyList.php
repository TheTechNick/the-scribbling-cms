<?php
	//sample of an contentelement
?>
<div class="my-list">
	<h5><?=$title?></h5>
	<ul>
		<?php foreach ($items as $item): ?>
		<li><?= $item ?></li>
		<?php endforeach ?>
	</ul>
</div>
