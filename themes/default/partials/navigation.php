<?php

use cms\helper\UrlManager;

$pages = [
	UrlManager::urlToPage(''),
	UrlManager::urlToPage('page'),
	UrlManager::urlToPage('page/subpage')
];

?>
<div class="header">
	<ul class="nav nav-pills pull-right">
		<?php foreach($pages as $page):?>
			<li class="<?php if ($page->path == $this->page->path) {
				echo "active";
			} ?>">
				<a href="<?=$page->url?>"><?=$page->meta['Title']?></a>
			</li>
		<?php endforeach?>
	</ul>
	<h3 class="text-muted"><?=$this->page->meta['Title']?></h3>
</div>