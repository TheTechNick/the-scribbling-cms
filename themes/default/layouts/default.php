<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="<?=$this->meta['description']?>">
		<meta name="author" content="<?=$this->meta['author']?>">
		<base href="<?=$config['base_url']?>">

		<title>CMS - <?=$this->page->meta['Title']?></title>

		<link rel="stylesheet" href="themes/default/res/bootstrap-3.2.0/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="themes/default/res/font-awesome-4.2.0/css/font-awesome.min.css">

		<link href="themes/default/res/jumbotron-narrow.css" rel="stylesheet">
	</head>

	<body>
		<div class="container">
			<?=$this->insertPartial('navigation')?>

			<?=$content?>

			<div class="footer">
				<div class="row">
					<div class="col-xs-6">
						<p>&copy; The Scribbling CMS by Nico Schieder 2014</p>
					</div>
					<div class="col-xs-6">
						<?php if($language == 'de'):?>
							<p><a href="<?=$this->page->urlForLanguage('en')?>">English version</a></p>
						<?php endif?>
						<?php if($language == 'en'):?>
							<p><a href="<?=$this->page->urlForLanguage('de')?>">Deutsche Version</a></p>
						<?php endif?>
					</div>
				</div>
			</div>

		</div>
	</body>
</html>
